// NOTE: CRUD operations
// CRUD operations is the heart of any backend application
// masterting the CRUD operations is essential for any developer.
// This helps in building character and increasing exposure to logical statements
// that will help us manipulate.
//mastering the CRUD operations of any languages makes us a valuable developer and makes the work
// easier for us to deal with huge amounts of information

// NOTE: [SECTIOn] Create (Inserting documenbt) :
// since MongoDB deals with object as it's structure for documents, we can easily create them by providing
//  objects into our methods.
// mongoDB shell also users javascript for it's javascript for its syntax which makes it
// convenient for us to understand it's code.

// Insert One document
/*
    Syntax:
        db.collectionName.insetOne({
          object/document
        })
 */

db.users.insertOne({
  firstName: 'Jane',
  lastName: 'Doe',
  age: 21,
  contact: {
    phone: '1234567890',
    email: 'janedoe@gmail.com',
  },
  courses: ['CSS', 'Javascript', 'Python'],
  department: 'none',
});

// Insert Many
/*
  db.user.insertMany([{objectA},
  {objectB}])
*/

db.user.insertMany([
  {
    firstName: 'Stephen',
    lastName: 'Hawking',
    age: 76,
    contact: {
      phone: '87654321',
      email: 'stephenhawking@gmail.com',
    },
    courses: ['Python', 'React', 'PHP'],
    department: 'none',
  },
  {
    firstName: 'Neil',
    lastName: 'Armstrong',
    age: 82,
    contact: {
      phone: '87654321',
      email: 'neilarmstrong@gmail.com',
    },
    courses: ['React', 'Lavravel', 'Sass'],
    department: 'none',
  },
]);

db.users.insertOne({
  firstName: 'Jane',
  lastName: 'Doe',
  age: 21,
  contact: {
    phone: '1234567890',
    email: 'janedoe@gmail.com',
  },
  courses: ['CSS', 'Javascript', 'Python'],
});

//NOTE: [SECTION] Find (read)

/*
  Syntax:
    it will show us all the documents in our collection
    db.collectionName.find();
    it will show us all the documents that has the given fieldset
    db.collectionName.find({field: value})

*/

db.users.find();

db.users.find({ age: 76 });

db.users.find({ firstName: 'jane' });

// finding documents using multiple fieldsets
/*
    Syntax:

    db.collectionname.fidn({fiendA: valueA, fieldB: valueB})
*/

db.users.find({ lastName: 'Armstrong', age: 82 });

// NOTE: [SECTION] updating documehnts

db.users.insertOne({
  firstName: 'Test',
  lastName: 'Test',
  age: 0,
  contact: {
    phone: '0000000',
    email: 'teest@gmail.com',
  },
  course: [],
  department: 'none',
});

//updateOne

/*
  Snytax:
  db.colletionName.UpdateOne({citeria}, {$set: {field:value}})
*/

db.users.updateOne(
  { firstName: 'Jane' },
  {
    $set: { lastName: 'Hawking' },
  }
);

//updating multiple documents

/*
    Snytax:
    db.collectionName.UpdateMany({criteria}, {
      $set: {
        field: value
      }
    })
*/

db.users.updateMany(
  { firstName: 'Jane' },
  {
    $set: {
      lastName: 'Wick',
      age: 25,
    },
  }
);

db.users.updateMany(
  { department: 'none' },
  {
    $set: {
      department: 'HR',
    },
  }
);

//replacing the old document

/*
WHOLE DOCUMENT WILL BE REPLACE
  db.users.repalceOne({criteria}, {document/objectToReplace})
*/

db.users.repalceOne(
  { firstName: 'Test' },
  {
    firstName: 'Gabriel',
    lastName: 'Mortel',
  }
);

// NOTE: [SECTION] Deleteing signle documents

/*
    db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({ firstName: 'Jane' });

//Delete many
/*
    db.collectionName.deleteMany({criteria});
*/

// reminded dont forget to put criteria
// NOTE: DO IT LIKE THIS
// db.collectionName.deleteMany({ firstName: 'Jane' });
// NOTE: IF YOU DONT PUT CRITERIA ALL DATA WILL BE DELETED
// db.users.deleteMany({});

//NOTE: [SECTION] Advanced queries
// embedded -  objects
//nested field - array

//query on an embedded
db.users.find({ 'contact.phone': '87654321' });

//query an array with exact elements
db.users.find({ courses: ['React', 'Lavravel', 'Sass'] });

//querying an array without regards to order and elements

db.users.find({ courses: { $all: ['React'] } });
